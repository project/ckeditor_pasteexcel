﻿(function () {
  'use strict';

  CKEDITOR.plugins.add('pasteexcel', {
    lang: 'en',
    init: function (editor) {

      editor.on('afterPasteFromWord', function (evt) {

        if (evt.data.dataValue.match(/^Version:1.0 StartHTML:/)) {
          evt.data.dataValue = evt.data.dataValue.substring(99);
        }
        if (!evt.data.dataValue.match(/<table/i)) {
          return;
        }

        var data = evt.data,
                tempDoc = document.implementation.createHTMLDocument(''),
                item, items,
                temp = new CKEDITOR.dom.element(tempDoc.body),
                tags = ['table', 'td', 'tr', 'col', 'colgroup'];

        temp.data('cke-editable', 1);
        temp.appendHtml(data.dataValue);

        _.each(tags, function (tag) {
          items = temp.find(tag);
          for (var i = 0; i < items.count(); i++) {
            item = items.getItem(i);
            item.removeStyle('width');
            item.removeAttribute('width');
          }
        });

        data.dataValue = temp.getHtml();
      });
    }

  });

})();
